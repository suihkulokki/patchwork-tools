#!/usr/bin/python
""" A script to generate a projects.json file from Patchworks.

    This script should be run with the following exports:
    export PYTHONPATH=$PYTHONPATH:../project:/srv/linaro-git-tools
    export DJANGO_SETTINGS_MODULE=local_settings
"""

import django
import subprocess
from patchwork.models import Project

django.setup()

for proj in Project.objects.all():
    if (
        "kernel.org" in proj.listemail
        or "alsa-devel" in proj.name
        or "qemu-devel" in proj.name
    ):
        subprocess.call(
            [
                "/srv/patches.linaro.org/tools/import_mbox.py",
                "--mbox_repo",
                proj.linkname,
                "--log",
                "INFO",
                "--days",
                "2",
            ]
        )
