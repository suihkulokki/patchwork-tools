FROM linaro/jenkins-amd64-ubuntu:bionic

RUN apt update \
  && DEBIAN_FRONTEND=noninteractive apt install -y --no-install-recommends \
  python-dev \
  python-virtualenv \
  virtualenv \
  python-flake8 \
  flake8 \
  libldap2-dev \
  libsasl2-dev \
  libssl-dev

COPY . patchwork-tools
RUN cd patchwork-tools \
&& ./unit-test.sh

CMD ["/usr/sbin/sshd", "-D"]
