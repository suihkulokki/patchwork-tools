import email
import os

from django.test import TestCase

import patch_matcher

_here = os.path.dirname(__file__)


class TestPatchMatcher(TestCase):
    def test_patches_similar_match(self):
        n1 = 'foo bar patch'
        n2 = 'foo bar patch.'
        d1 = 'patch content patch conent'
        d2 = 'patch content patch conent.'
        self.assertTrue(patch_matcher._patches_similar(n1, d1, n2, d2))

    def test_patches_similar_title_miss(self):
        n1 = 'foo bar patch'
        n2 = 'FO bar pa'
        d1 = 'patch content patch conent'
        d2 = 'patch content patch conent'
        self.assertFalse(patch_matcher._patches_similar(n1, d1, n2, d2))

    def test_patches_similar_diff_miss(self):
        n1 = 'foo bar patch'
        n2 = 'foo bar patch.'
        d1 = 'patch content patch conent'
        d2 = 'patch content patch'
        self.assertFalse(patch_matcher._patches_similar(n1, d1, n2, d2))

    def test_patches_similar_revert(self):
        n2 = 'foo bar patch' * 5
        n1 = 'Revert "%s"' % n2
        d1 = 'patch content patch conent'
        self.assertFalse(patch_matcher._patches_similar(n1, d1, n2, d1))

    def _load_patch(self, name):
        with open(os.path.join(_here, 'data', name)) as f:
            msg = f.read()
        msg = email.message_from_string(msg)
        name = msg.get('Subject').split(']')[1]
        diff = msg.get_payload(decode=True)
        return name, diff

    def test_real_patch(self):
        '''test a real patch match'''
        n1, d1 = self._load_patch('odp_1781.mbox')
        n2, d2 = self._load_patch('odp_1792.mbox')
        self.assertTrue(patch_matcher._patches_similar(n1, d1, n2, d2))

    def test_patches_similar_high_title(self):
        '''checks for similiarity should be less restrictive by content if the
        patch titles are basically identical'''
        n1, d1 = self._load_patch('odp_1792.mbox')
        n2, d2 = self._load_patch('odp_2040.mbox')
        self.assertTrue(patch_matcher._patches_similar(n1, d1, n2, d2))
