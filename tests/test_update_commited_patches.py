import os
import shutil
import tempfile
import time

import import_emails
import update_commited_patches

from django.test import TestCase
from patchwork.models import Patch, Project, State
from linaro_metrics.models import (
    CommitTagCredit,
    TeamMembership,
    Team,
)
from linaro_metrics.parsemail import get_linaro_person
from tests import TestRepo

import mock

from linaro_metrics.team_project_credit import update_commit_callback


class TestUpdateCommitedPatches(TestCase):
    '''This class does a bunch of mocking and setup, but the code it tests is
    is complex enough to make this quite useful for sanity checking'''
    fixtures = ['default_states']

    def setUp(self):
        self.tmpdir = tempfile.mkdtemp()
        self.addCleanup(shutil.rmtree, self.tmpdir)
        path = os.path.join(self.tmpdir, 'upstream')
        self.upstream_repo = TestRepo.create(path)
        self.upstream_repo.add_commit('file.txt', 'linel', 'commit 1')
        self.upstream_repo.add_commit('file.txt', 'line1', 'commit 2')

        path = os.path.join(self.tmpdir, 'patchwork_copy')
        self.local_repo = TestRepo.clone(self.upstream_repo.path, path, True)

        self.project = Project.objects.create(
            listid='lng-odp.lists.linaro.org',
            linkname='patchwork_copy')
        self.addCleanup(self.project.delete)

        commit = self.upstream_repo.add_commit(
            'file.txt', 'line1\nline2', 'testing update_commited_patches')
        self.patchmail = self.upstream_repo.create_patch(commit)

    def test_update_project(self):
        '''Ensure an exact patch match gets marked accepted'''
        # create a patch for our project
        with open(self.patchmail) as f:
            mail = 'Message-Id: 1234\nList-Id: %s\n' % self.project.listid
            mail += f.read()
        import_emails.process_message(mail)
        patches = Patch.objects.all()
        self.assertEqual(1, patches.count())
        self.assertEqual(State.objects.get(name='New'), patches[0].state)

        update_commited_patches._update_project(
            None, self.tmpdir, self.project, [], False)

        patches = Patch.objects.all()
        self.assertEqual(1, patches.count())
        self.assertEqual(State.objects.get(name='Accepted'), patches[0].state)

    def test_update_project_fuzzy(self):
        '''Ensure a patch that's a close match is marked accepted'''
        # create a patch for our project
        with open(self.patchmail) as f:
            mail = 'Message-Id: 1234\nList-Id: %s\n' % self.project.listid
            mail += f.read()
        mail = mail.replace('+line2', '+Line2##')  # make this an inexact patch
        import_emails.process_message(mail)
        patches = Patch.objects.all()
        self.assertEqual(1, patches.count())
        self.assertEqual(State.objects.get(name='New'), patches[0].state)

        update_commited_patches._update_project(
            None, self.tmpdir, self.project, [], False)

        patches = Patch.objects.all()
        self.assertEqual(1, patches.count())
        self.assertEqual(State.objects.get(name='Accepted'), patches[0].state)

    def test_update_by_submitter(self):
        '''We track patches *submitted* by Linaro but not authored. These need
           to be included for updating'''
        with open(self.patchmail) as f:
            mail = 'Message-Id: 1234\nList-Id: %s\n' % self.project.listid
            mail += f.read()
        import_emails.process_message(mail)
        patches = Patch.objects.all()
        self.assertEqual(1, patches.count())
        self.assertEqual(State.objects.get(name='New'), patches[0].state)

        # update author to something different 'foo@bar.com'
        self.upstream_repo.run_command(
            ['git', 'commit', '--amend', '--no-edit',
             '--author=foo bar <foo@bar.com>'])
        update_commited_patches._update_project(
            None, self.tmpdir, self.project, [], False)

        patches = Patch.objects.all()
        self.assertEqual(1, patches.count())
        self.assertEqual(State.objects.get(name='Accepted'), patches[0].state)

    def test_team_project_credit(self):
        '''Ensure a git tag credit gets added'''

        commit = mock.Mock()
        commit.message = 'Commit message\n' \
                         'Signed-off-by: Foo Bar <foo.bar@linaro.org>'
        commit.id = '1234'
        commit.author = 'Foo Bar <foo.bar@linaro.org>'
        commit.commit_time = time.time()
        commit.commit_timezone = -30
        crowd = mock.Mock()
        crowd.user_valid.return_value = True

        crowd.get_user_no_cache.return_value = {
            'display-name': 'foo.bar@linaro.org',
            'email': 'foo.bar@linaro.org',
        }

        p = get_linaro_person(crowd, 'foo.bar@linaro.org').user
        t = Team.objects.create(name='TeamName')
        TeamMembership.objects.create(team=t, user=p)

        update_commit_callback(crowd, self.project, None, commit, False)

        credits = CommitTagCredit.objects.all()
        self.assertEqual(1, credits.count())
        credit = CommitTagCredit.objects.get(tag='Signed-off')
        self.assertEqual('1234', credit.commit)
        self.assertFalse(credit.non_author)
