import os
import subprocess


class TestRepo(object):
    '''Simple class to make it easy to create/manipulate test repos'''

    @staticmethod
    def create(path):
        os.mkdir(path)
        subprocess.check_call(['git', 'init'], cwd=path)
        return TestRepo(path)

    @staticmethod
    def clone(repo_url, path, mirror=False):
        args = ['git', 'clone']
        if mirror:
            args.append('--mirror')
        args.append(repo_url)
        args.append(path)
        subprocess.check_call(args)
        return TestRepo(path)

    def __init__(self, path):
        self.path = path

    def add_commit(self, fname, content, message):
        with open(os.path.join(self.path, fname), 'w') as f:
            f.write(content)
        subprocess.check_call(['git', 'add', '.'], cwd=self.path)
        subprocess.check_call(
            ['git', 'commit', '-a', '-m', message], cwd=self.path)
        return self.last_commit()

    def run_command(self, args):
        subprocess.check_call(args, cwd=self.path)

    def last_commit(self):
        out = subprocess.check_output(
            ['git', 'log', '--format=oneline', '-1'], cwd=self.path
        ).decode("utf-8")
        return out.split(' ')[0]

    def create_patch(self, commit):
        patch = subprocess.check_output(
            ['git', 'format-patch', '%s^..%s' % (commit, commit)],
            cwd=self.path).decode("utf-8")
        return os.path.join(self.path, patch.strip())
