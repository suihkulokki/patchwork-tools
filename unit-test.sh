#!/bin/sh -ex

HERE=$(dirname $(readlink -f $0))
cd $HERE

VENV_DIR="${VENV_DIR-$HERE/.venv}"

if [ -z $VIRTUAL_ENV ] ; then
	echo "creating venv: $VENV_DIR ..."
	virtualenv --python=`which python3` $VENV_DIR
	. $VENV_DIR/bin/activate
	pip3 install -r requirements.txt
#	wget -q https://git.linaro.org/infrastructure/linaro-git-tools.git/plain/linaro_ldap.py
fi

if [ -z $PYTHONPATH ] ; then
	if [ ! -d $VENV_DIR/patchwork ]; then
		git clone https://github.com/getpatchwork/patchwork.git $VENV_DIR/patchwork
		cd $VENV_DIR/patchwork
		git checkout -b production v2.2.3
		cd $HERE
	fi
	export PYTHONPATH=$VENV_DIR/patchwork
fi

echo
echo "== Running flake8 checks..."
echo
FLAKE_EXCLUDE="linaro_metrics/migrations,./linaro_ldap.py"
if echo $VENV_DIR | grep $HERE ; then
	# make sure we don't run tests on the venv if its in our source tree
	FLAKE_EXCLUDE="$VENV_DIR,$FLAKE_EXCLUDE"
fi
flake8 --ignore=E401,E402,W503 --show-source --exclude=$FLAKE_EXCLUDE ./

echo
echo "== Running test suite..."
echo
if [ ! -f ~/.gitconfig ] ; then
	git config --global user.email "citestbot@example.com"
	git config --global user.name "ci test bot"
fi
export DJANGO_SETTINGS_MODULE=linaro_metrics.settings
export PYTHONPATH=./:./tests:$PYTHONPATH

# find the manage.py script
for x in $(echo $PYTHONPATH | sed -e 's/:/\n/g') ; do
	if [ -f $x/manage.py ] ; then
		MANAGE=$x/manage.py
	fi
done
if [ -z $MANAGE ] ; then
	echo "ERROR: no manage.py found in $PYTHONPATH"
	exit 1
fi
$MANAGE test $*
