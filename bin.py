import argparse
import logging
import os
import sys

import django


def django_setup():
    try:
        import patchwork
    except Exception:
        # try and guess location based on how ansible deploys patchwork
        here = os.path.abspath(os.path.dirname(__file__))
        patchwork = os.path.join(here, '../project')
        if not os.path.exists(patchwork):
            sys.exit('patchwork not found in PYTHONPATH or at: %s' % patchwork)
        sys.path.append(patchwork)

    if 'DJANGO_SETTINGS_MODULE' not in os.environ:
        os.environ['DJANGO_SETTINGS_MODULE'] = 'local_settings'

    if getattr(django, 'setup', None):
        django.setup()


def _init_logging(level):
    handler = logging.StreamHandler(stream=sys.stderr)
    formatter = logging.Formatter(
        '%(asctime)s %(levelname)-5s %(name)s: %(message)s',
        datefmt='%H:%M:%S')
    handler.setFormatter(formatter)
    log = logging.getLogger('')
    log.addHandler(handler)
    log.setLevel(getattr(logging, level))


class LoggingAction(argparse.Action):
    def __call__(self, parser, namespace, value, option_string=None):
        log = logging.getLogger()
        log.setLevel(getattr(logging, value))
        log.debug('logging level set to: %s', value)


def add_logging_arguments(parser, level='WARN'):
    _init_logging(level)
    parser.add_argument('--log', default=level, action=LoggingAction,
                        choices=('WARN', 'INFO', 'DEBUG'),
                        help='Logging level to use. Default=%(default)s')
