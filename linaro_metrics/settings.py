import os
from patchwork.settings.base import *  # NOQA

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'licenses.db',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}

TIME_ZONE = 'UTC'
LANGUAGE_CODE = 'en-us'

ROOT_URLCONF = 'linaro_metrics.urls'

linaro_htdocs = os.path.join(
    os.path.dirname(os.path.abspath(__file__)), 'htdocs')
STATICFILES_DIRS = [
    linaro_htdocs,
    os.path.join(ROOT_DIR, 'htdocs'),  # NOQA F405
]

SECRET_KEY = '00000000000000000000000000000000000000000000000000'
DEBUG = True
TEMPLATE_DEBUG = True

CROWD_USER = None
CROWD_PASS = None
CROWD_URL = None
CROWD_CACHE = '/tmp/crowd_cache.json'

INSTALLED_APPS.append('linaro_metrics')  # NOQA F405
DEFAULT_TEAM = 'no-team'
DEFAULT_PROJECT = 'no-project'

PARSEMAIL_MONKEY_PATCHER = 'linaro_metrics.parsemail.monkey_patcher'

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': '/tmp/patches-cache',
        'TIMEOUT': 60 * 60,  # 1 hour cache
    }
}
