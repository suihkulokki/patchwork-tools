#!/usr/bin/python

import argparse
import logging
import os
import sys
import time

import psycopg2.extras

from bin import django_setup, add_logging_arguments


from django.db import connection
from django.db.utils import ConnectionHandler

from django.contrib.auth.models import User

from patchwork.models import (
    Bundle,
    Comment,
    Patch,
    Person,
    Project,
    State,
)

from linaro_metrics.sync_gerrit_changes import get_patch_content
from linaro_metrics.models import Team, TeamCredit, TeamMembership

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
django_setup()  # Must be called to get sys.path and django settings in place.
log = logging.getLogger('db-migrate')


def table_iterator(cursor, table):
    row = None
    try:
        old_cursor.execute('SELECT * from %s' % table)
        for row in old_cursor:
            yield row
    except Exception:
        if row:
            print('failed on row: %s' % row)
        raise


def restore_seq(clazz, seq):
    '''Ensure the auto incrementing pk is fixed.'''
    cur = connection.cursor()
    cur.execute('SELECT setval(\'%s\', (SELECT MAX(id) FROM %s))' % (
        seq, clazz._meta.db_table))
    cur.close()


def restore_table(old_cursor, clazz, table=None, seq=None):
    '''Import an old table as-is.'''
    if not table:
        table = clazz._meta.db_table
    log.info('Restoring table %s', table)
    for row in table_iterator(old_cursor, table):
        clazz(**row).save()

    if seq:
        restore_seq(clazz, seq)


def migrate_memberships(old_cursor):
    log.info('Migrating memberships')
    for row in table_iterator(old_cursor, 'patchmetrics_teammembership'):
        TeamMembership(user_id=row['member_id'], team_id=row['team_id']).save()


def migrate_projects(old_cursor):
    log.info('Migrating projects')
    for row in table_iterator(old_cursor, 'patchwork_project'):
        web_url = row['commit_url']
        if not web_url:
            web_url = 'n/a'
        scm_url = row['source_tree']
        if not scm_url:
            scm_url = 'n/a'

        log.debug('creating project: %s', row['linkname'])
        Project(
            id=row['id'],
            linkname=row['linkname'],
            name=row['name'],
            listid=row['listid'],
            listemail=row['listemail'],
            scm_url=scm_url,
            webscm_url=web_url,
            web_url=web_url,
        ).save()
    restore_seq(Project, 'patchwork_project_id_seq')


def migrate_patches(old_cursor):
    log.info('Migrating patches')
    for row in table_iterator(old_cursor, 'patchwork_patch'):
        # Old patch has 2 columns we don't support.
        args = {k: v for k, v in list(row.items())
                if k not in ('date_last_state_change', 'author_id')}
        p = Patch(**args)
        p.linaro_author = Person.objects.get(pk=row['author_id'])
        p.save()

        for tc in TeamCredit.objects.filter(patch=row['id']):
            tc.last_state_change = row['date_last_state_change']
            tc.save()
    restore_seq(Patch, 'patchwork_patch_id_seq')


def migrate_gerrit(old_cursor, cursor2):
    # We don't need a GerritChange model. We just need some naming pattern
    # to look up changes from gerrit. This is pretty easy by using the
    # the "msgid" field which is unique.

    log.info('Migrating gerrit changes')
    for row in table_iterator(old_cursor, 'patchmetrics_gerritchange'):
        p = Patch.objects.get(pk=row['patch_id'])
        change_id = row['change_id']
        if change_id[0] != 'I':
            # We have a few old patches with extra stuff in this field.
            idx = change_id.rfind('~I')
            change_id = change_id[idx + 1:]
        p.msgid = '%s@%s' % (row['gerrit_id'], change_id)

        # Fake our sync_gerrit_helper with data needed to generate content
        # find the original project name:
        cursor2.execute('SELECT * from patchwork_patch WHERE id = %d' % p.id)
        proj_id = cursor2.fetchone()['project_id']
        cursor2.execute(
            'SELECT name from patchwork_project where id = %d' % proj_id)
        aosp_proj = cursor2.fetchone()['name']

        fake_change = {'project': aosp_proj, '_number': row['gerrit_id']}
        p.content = get_patch_content(fake_change)
        p.save()

    # Now move all patches to AOSP and delete the old ones
    new_p = Project.objects.create(
        name='AOSP', linkname='aosp', listid='n/a for AOSP',
        listemail='https://android-review.googlesource.com/')
    for p in Project.objects.filter(name__startswith='AOSP '):
        p.patch_set.update(project=new_p)
        p.delete()


if __name__ == '__main__':
    DEF_OPS = (
        'User', 'State', 'Person', 'Team', 'Project', 'Membership', 'Patch',
        'Comment', 'Bundle', 'GerritChange',
    )

    parser = argparse.ArgumentParser(
        description='''Import data from old patchmetrics database into the new
                    database.''')
    parser.add_argument('db_object', default=DEF_OPS, nargs='*',
                        help='ORM objects to migration. default=%(default)s')
    add_logging_arguments(parser, level='INFO')
    args = parser.parse_args()

    ch = ConnectionHandler()

    # Causes underlying connection so we can then construct a DictCursor
    # a DictCursor makes it easier to deal with each table row.
    ch['orig'].cursor().close()
    old_cursor = ch['orig'].connection.cursor(
        cursor_factory=psycopg2.extras.DictCursor)

    start = time.time()

    if 'User' in args.db_object:
        restore_table(old_cursor, User, seq='auth_user_id_seq')
    if 'State' in args.db_object:
        restore_table(old_cursor, State, seq='patchwork_state_id_seq')
    if 'Person' in args.db_object:
        restore_table(old_cursor, Person, seq='patchwork_person_id_seq')
    if 'Team' in args.db_object:
        restore_table(old_cursor, Team, 'patchmetrics_team',
                      'linaro_metrics_team_id_seq')

    if 'Project' in args.db_object:
        migrate_projects(old_cursor)

    if 'Membership' in args.db_object:
        migrate_memberships(old_cursor)
    if 'Patch' in args.db_object:
        migrate_patches(old_cursor)

    if 'Comment' in args.db_object:
        restore_table(old_cursor, Comment, seq='patchwork_comment_id_seq')
    if 'Bundle' in args.db_object:
        restore_table(old_cursor, Bundle, seq='patchwork_bundle_id_seq')
    if 'GerritChange' in args.db_object:
        cursor2 = ch['orig'].connection.cursor(
            cursor_factory=psycopg2.extras.DictCursor)
        migrate_gerrit(old_cursor, cursor2)

    log.info('Migrations took: %d seconds', (time.time() - start))
