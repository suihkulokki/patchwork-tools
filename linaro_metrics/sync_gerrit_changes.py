#!/usr/bin/env python
import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from bin import django_setup, add_logging_arguments
django_setup()  # must be called to get sys.path and django settings in place

import json
import logging
import textwrap
import urllib.request, urllib.parse, urllib.error
import urllib.request, urllib.error, urllib.parse

from datetime import datetime

from patchwork.models import Person, Patch, Project, State

from linaro_metrics.models import TeamCredit, TeamMembership


STATE_MAP = {'MERGED': 'Accepted', 'NEW': 'New', 'ABANDONED': 'Rejected'}
log = logging.getLogger('sync_gerrit_changes')

GERRIT_PROJECTS = {
    'AOSP': 'https://android-review.googlesource.com',
    'openocd': 'http://openocd.zylin.com',
    'OpenStack': 'https://review.openstack.org',
    'OpenDaylight': 'https://git.opendaylight.org/gerrit',
    'westeros': 'https://code.rdkcentral.com/r',
    'fd.io': 'https://gerrit.fd.io/r',
}


def get_user_changes(email, url_base, offset=0):
    params = {
        'n': 25,
        'q': 'owner:' + email,
        'start': offset,
    }
    url = url_base + '/changes/?' + urllib.parse.urlencode(params)
    log.debug('doing http get on: %s', url)
    try:
        resp = urllib.request.urlopen(url)
        resp = resp.read()
        assert resp.startswith(")]}'")
        entries = json.loads(resp[4:])
        e = None
        for e in entries:
            yield e
        if e and e.get('_more_changes'):
            for e in get_user_changes(email, url_base, offset + len(entries)):
                yield e
    except urllib.error.HTTPError as e:
        if e.code != 400:
            log.exception('Unable to GET: %s', url)
            sys.exit(1)


def patchwork_state(gerrit_status):
    return State.objects.get(name=STATE_MAP[gerrit_status])


def get_patch_content(url_base, project, change):
    fmt = textwrap.dedent('''\
        Project: %s
        %s/#/c/%d/

        This represents an %s change submitted via Gerrit. It is mirrored
        here so that it is included in our statistics.''')
    return fmt % (change['project'], url_base, change['_number'], project)


def create_or_update(url_base, project, email, change):
    changed = False
    msgid = '%s@%s' % (change['_number'], change['change_id'])
    created = datetime.strptime(change['created'], '%Y-%m-%d %H:%M:%S.%f000')
    updated = datetime.strptime(change['updated'], '%Y-%m-%d %H:%M:%S.%f000')
    fields = {
        'name': change['subject'],
        'project': Project.objects.get(name=project),
        'state': patchwork_state(change['status']),
    }
    try:
        p = Patch.objects.get(msgid=msgid)
        tcs = TeamCredit.objects.filter(patch=p)
        if tcs.count() == 0:
            # legacy patch from a non-linaro person, no use tracking anymore
            log.warn('non-linaro user should be removed: %s', email)
            return changed
        if updated > tcs[0].last_state_change:
            for k, v in fields.items():
                setattr(p, k, v)
            changed = True
            p.save()
            TeamCredit.objects.filter(patch=p).update(
                last_state_change=updated)
    except Patch.DoesNotExist:
        changed = True
        fields['msgid'] = msgid
        fields['date'] = created
        fields['submitter'] = Person.objects.get(email=email)
        fields['content'] = get_patch_content(url_base, project, change)
        # NOTE: the Patch class has overridden "save" incorrectly, so we
        # can't call Patch.objects.create()
        p = Patch(**fields)
        p.save()
        # teamcredits are auto-set to "now", so we need to update it to what
        # came from gerrit
        TeamCredit.objects.filter(patch=p).update(last_state_change=updated)
    return changed


def sync_user_changes(email):
    log.debug('syncing changes for: %s', email)
    changes = 0

    for project, url_base in list(GERRIT_PROJECTS.items()):
        for change in get_user_changes(email, url_base):
            log.debug('checking change: %s', change['change_id'])
            if not create_or_update(url_base, project, email, change):
                break
            changes += 1
        if changes:
            log.info('updated %d patches for: %s', changes, email)


def get_linaro_persons():
    users_query = TeamMembership.objects.values('user').distinct().query
    return Person.objects.filter(user__id__in=users_query)


def main(args):
    if args.email:
        for email in args.email:
            sync_user_changes(email)
    else:
        for person in get_linaro_persons():
            sync_user_changes(person.email)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(
        description='Synchronize Linaro changes from AOSP')
    add_logging_arguments(parser)
    parser.add_argument('email', nargs='*', help='only check for given emails')
    args = parser.parse_args()
    main(args)
