import contextlib
import datetime
import functools
import re
import logging

from django.conf import settings

from linaro_metrics.crowd import Crowd
from linaro_metrics.parsemail import get_linaro_person
from linaro_metrics.models import (
    CommitTagCredit,
    TeamTagCredit,
    TeamMembership,
    ProjectTagCredit,
)

log = logging.getLogger('update_commited_patches')

response_re = \
    re.compile(r'^(Tested|Reviewed|Acked|Signed-off|Nacked|Reported)-by:'
               r' .*<(.*@.*)>$', re.M | re.I)


def update_commit_callback(crowd, project, repo, commit, dryrun):
    for match in response_re.finditer(commit.message):
        tag, email = match.groups()
        p = get_linaro_person(crowd, email)
        if p:
            log.debug('User %s found with tag %s', p, tag)
            if not dryrun:
                ts = datetime.datetime.fromtimestamp(
                    commit.commit_time + commit.commit_timezone)
                non_author = email not in commit.author
                for m in TeamMembership.objects.filter(user=p.user):
                    ctc, created = CommitTagCredit.objects.get_or_create(
                        tag=tag, person=p, commit=commit.id,
                        defaults={'date': ts, 'non_author': non_author},
                    )
                    ProjectTagCredit.objects.get_or_create(
                        commit_tag=ctc, project=project
                    )
                    TeamTagCredit.objects.get_or_create(
                        commit_tag=ctc, team=m.team
                    )


@contextlib.contextmanager
def update_commit_callback_constructor():
    crwd = Crowd(settings.CROWD_USER, settings.CROWD_PASS, settings.CROWD_URL)

    with crwd.cached(settings.CROWD_CACHE):
        cb = functools.partial(update_commit_callback, crwd)
        yield cb
