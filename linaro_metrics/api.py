from django.urls import reverse
from django.shortcuts import get_object_or_404

from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.serializers import (
    HyperlinkedModelSerializer, SerializerMethodField, ModelSerializer)


from patchwork.api.patch import PatchListSerializer
from patchwork.models import Patch

from linaro_metrics.models import Team, TeamTagCredit


class TeamListSerializer(HyperlinkedModelSerializer):
    patches = SerializerMethodField()
    tags = SerializerMethodField()

    def get_patches(self, instance):
        return self.context.get('request').build_absolute_uri(
            reverse('api-team-patch-list', kwargs={'team_id': instance.id}))

    def get_tags(self, instance):
        return self.context.get('request').build_absolute_uri(
            reverse('api-team-credits', kwargs={'team_id': instance.id}))

    class Meta:
        model = Team
        fields = \
            ('id', 'url', 'name', 'display_name', 'active', 'patches', 'tags')
        read_only_fields = fields
        extra_kwargs = {
            'url': {'view_name': 'api-team-detail'},
        }


class TeamList(ListAPIView):
    serializer_class = TeamListSerializer
    search_fields = ('name', 'display_name')
    ordering_fields = ('name', 'display_name', 'active')

    def get_queryset(self):
        return Team.objects.all()


class TeamDetail(RetrieveAPIView):
    serializer_class = TeamListSerializer

    def get_queryset(self):
        return Team.objects.all()

    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())

        try:
            obj = queryset.get(id=int(self.kwargs['pk']))
        except (ValueError, Team.DoesNotExist):
            obj = get_object_or_404(queryset, name=self.kwargs['pk'])

        self.kwargs['pk'] = obj.id
        self.check_object_permissions(self.request, obj)
        return obj


class TeamPatchList(ListAPIView):
    lookup_url_kwarg = 'team_id'
    serializer_class = PatchListSerializer
    queryset = Patch.objects.all()

    def get_queryset(self):
        try:
            obj = Team.objects.get(id=int(self.kwargs['team_id']))
        except (ValueError, Team.DoesNotExist):
            obj = get_object_or_404(Team, name=self.kwargs['team_id'])
        return Patch.objects.filter(teamcredit__team=obj)


class TeamTagListSerializer(ModelSerializer):
    person = SerializerMethodField()
    tag = SerializerMethodField()
    commit = SerializerMethodField()

    def get_person(self, instance):
        return instance.commit_tag.person.name

    def get_tag(self, instance):
        return instance.commit_tag.tag

    def get_commit(self, instance):
        return instance.commit_tag.commit

    class Meta:
        model = TeamTagCredit
        exclude = ('commit_tag', 'id', 'team')


class TeamTagView(ListAPIView):
    lookup_url_kwarg = 'team_id'
    serializer_class = TeamTagListSerializer

    def get_queryset(self):
        try:
            obj = Team.objects.get(id=int(self.kwargs['team_id']))
        except (ValueError, Team.DoesNotExist):
            obj = get_object_or_404(Team, name=self.kwargs['team_id'])
        return TeamTagCredit.objects.filter(team=obj).select_related(
            'commit_tag'
        )
