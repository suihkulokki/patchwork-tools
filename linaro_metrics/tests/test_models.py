import datetime
import os

import import_emails

from django.conf import settings
from django.contrib.auth.models import User
from django.test import TestCase, skipUnlessDBFeature
from patchwork.models import Patch, Person, Project, State
from linaro_metrics.models import (
    _patch_author,
    Team,
    TeamCredit,
    TeamMembership,
)


class TestModels(TestCase):
    fixtures = ['default_states']

    def setUp(self):
        self.proj = Project.objects.create(
            linkname='lng-odp', listid='lng-odp.lists.linaro.org')
        self.data_dir = os.path.join(
            os.path.dirname(__file__), '../../tests/data')

    def test_patch_callbacks(self):
        '''Ensure the model responds to callback changes from Patch objects'''
        with open(os.path.join(self.data_dir, 'odp_1781.mbox')) as f:
            mail = f.read()
        import_emails.process_message(mail)

        # patch should be credited to default team
        patches = Patch.objects.all()
        self.assertEqual(1, patches.count())
        tcs = patches[0].teamcredit_set.all()
        self.assertEqual(1, tcs.count())
        self.assertEqual(settings.DEFAULT_TEAM, tcs[0].team.name)

        # and a change to should increment the last_state_change
        ts = tcs[0].last_state_change
        patches[0].save()
        tcs = patches[0].teamcredit_set.all()
        self.assertGreater(tcs[0].last_state_change, ts)

    def test_patch_author_linaro_person(self):
        '''user.email isn't linaro but the "person" is'''
        user = User(email='foo@bar.org')
        person = Person(email='foo@linaro.org')
        person.user = user
        patch = Patch(submitter=person)
        patch.author = person
        is_linaro, user = _patch_author(patch)
        self.assertTrue(is_linaro)
        self.assertEqual('foo@bar.org', user.email)

    def test_patch_author_linaro_user(self):
        '''user.email is linaro but the "person" is not'''
        user = User(email='foo@linaro.org')
        person = Person(email='foo@bar.org')
        person.user = user
        patch = Patch(submitter=person)
        patch.author = person
        is_linaro, user = _patch_author(patch)
        self.assertTrue(is_linaro)
        self.assertEqual('foo@linaro.org', user.email)

    def test_patch_author_submitter(self):
        '''Ensure we can find author if only handled via "submitter" field'''
        user = User(email='foo@linaro.org')
        person = Person(email='foo@bar.org')
        person.user = user
        patch = Patch(submitter=person)
        is_linaro, user = _patch_author(patch)
        self.assertTrue(is_linaro)
        self.assertEqual('foo@linaro.org', user.email)

    def test_patch_author_not_linaro(self):
        '''test that we can handle non-linaro authors
        This could happen if the submitter is linaro, but not the author.
        '''
        user = User(email='foo@foo.org')
        person = Person(email='foo@bar.org')
        person.user = user
        patch = Patch(submitter=person)
        is_linaro, user = _patch_author(patch)
        self.assertFalse(is_linaro)
        self.assertEqual('foo@foo.org', user.email)

    def test_patch_non_linaro_member(self):
        '''We have some teams that have non-linaro members. These patches
        should get credited properly'''
        user = User.objects.create(email='foo@foo.org')
        person = Person.objects.create(email='foo@bar.org', user=user)
        t = Team.objects.create(name='foo team')
        TeamMembership.objects.create(team=t, user=user)

        project = Project.objects.get(linkname='lng-odp')
        p = Patch(
            submitter=person, date=datetime.datetime.now(), project=project)
        p.save()
        tcs = p.teamcredit_set.all()
        self.assertEqual(1, tcs.count())
        self.assertEqual(t, tcs[0].team)
        self.assertEqual(p, tcs[0].patch)

    def test_patch_non_linaro(self):
        '''ensure that if a totally non-linaro patch slips through we don't
        credit it to a team'''
        user = User.objects.create(email='foo@foo.org')
        person = Person.objects.create(email='foo@bar.org', user=user)

        project = Project.objects.get(linkname='lng-odp')
        p = Patch(
            submitter=person, date=datetime.datetime.now(), project=project)
        p.save()
        tcs = p.teamcredit_set.all()
        self.assertEqual(0, tcs.count())

    def _create_patch(self, name, date, state):
        person, _ = Person.objects.get_or_create(email='testing@linaro.org')
        p = Patch(project=self.proj, name=name, date=date, submitter=person,
                  msgid=name, state=state)
        p.save()
        return p

    def test_get_month_metrics(self):
        def _last_month(ts):
            if ts.month > 1:
                return ts.replace(month=ts.month - 1)
            return ts.replace(year=ts.year - 1, month=12)

        new = State.objects.get(name='New')
        accepted = State.objects.get(name='Accepted')
        ts = datetime.datetime.now()
        self._create_patch('this month 1', ts, new)
        self._create_patch('this month 2', ts, new)
        self._create_patch('this month 3', ts, accepted)

        ts = _last_month(ts)
        self._create_patch('last month 1', ts, new)
        self._create_patch('last month 2', ts, new)

        ts = _last_month(ts)
        self._create_patch('2 months ago 1', ts, new)
        self._create_patch('2 months ago 2', ts, new)
        self._create_patch('2 months ago 3', ts, accepted)
        self._create_patch('2 months ago 4', ts, accepted)

        ts = _last_month(ts)
        self._create_patch('too many months ago 1', ts, new)

        counts = TeamCredit.patch_count_by_month(3)
        self.assertEqual(3, len(counts))
        self.assertEqual(4, counts[0]['patch__pk__count'])
        self.assertEqual(2, counts[1]['patch__pk__count'])
        self.assertEqual(3, counts[2]['patch__pk__count'])

        metrics = TeamCredit.get_month_metrics(3)
        self.assertEqual(3, len(metrics))
        self.assertEqual(4, metrics[0][1])
        self.assertEqual(2, metrics[0][2])

        self.assertEqual(2, metrics[1][1])
        self.assertEqual(0, metrics[1][2])

        self.assertEqual(3, metrics[2][1])
        self.assertEqual(1, metrics[2][2])

    # This can't be run against sqlite3, so it won't get hit by unit-test.sh.
    @skipUnlessDBFeature('supports_mixed_date_datetime_comparisons')
    def test_time_to_acceptance(self):
        accepted = State.objects.get(name='Accepted')

        # 1 patch < 7 days
        ts = datetime.datetime.now()
        self._create_patch('1 week', ts, accepted)

        # 2 patches < 14 days
        ts = datetime.datetime.now() - datetime.timedelta(days=10)
        self._create_patch('2 weeks 1', ts, accepted)
        self._create_patch('2 weeks 2', ts, accepted)

        # Leave a gap for 14-21 days

        # 4 patches < 28 days
        ts = datetime.datetime.now() - datetime.timedelta(days=22)
        self._create_patch('4 weeks 1', ts, accepted)
        self._create_patch('4 weeks 2', ts, accepted)
        self._create_patch('4 weeks 3', ts, accepted)
        self._create_patch('4 weeks 4', ts, accepted)

        # 2 patches > 42 days
        ts = datetime.datetime.now() - datetime.timedelta(days=50)
        self._create_patch('>42 1', ts, accepted)
        self._create_patch('>42 2', ts, accepted)

        tta = TeamCredit.time_to_acceptance()
        self.assertEqual(
            ([7, 1], [14, 2], [21, 0], [28, 4], [35, 0], [42, 2]), tta)

    # This can't be run against sqlite3, so it won't get hit by unit-test.sh.
    @skipUnlessDBFeature('supports_mixed_date_datetime_comparisons')
    def test_age_distribution(self):
        new = State.objects.get(name='New')

        # 1 patch = 0 days
        ts = datetime.datetime.now()
        self._create_patch('00days0', ts, new)

        # 2 patches <= 15 days
        ts = datetime.datetime.now() - datetime.timedelta(days=10)
        self._create_patch('10days1', ts, new)
        self._create_patch('10days2', ts, new)

        # 3 patches <= 30 days
        ts = datetime.datetime.now() - datetime.timedelta(days=30)
        self._create_patch('20days1', ts, new)
        self._create_patch('20days2', ts, new)
        self._create_patch('20days3', ts, new)

        # 4 patches <= 45 days
        ts = datetime.datetime.now() - datetime.timedelta(days=40)
        self._create_patch('40days1', ts, new)
        self._create_patch('40days2', ts, new)
        self._create_patch('40days3', ts, new)
        self._create_patch('40days4', ts, new)

        data = TeamCredit.age_distribution_data(15, 7)
        self.assertEqual(
            [[0, 1], [15, 2], [30, 3], [45, 4]], data)
