from django.contrib.auth.models import User
from django.test import Client, TestCase, skipUnlessDBFeature

from linaro_metrics.models import Team
from linaro_metrics.sync_teams import sync_crowd
from linaro_metrics.tests.test_sync_teams import FakeCrowd


class TestTeamsView(TestCase):
    fixtures = ['default_states']

    def setUp(self):
        # Create team memberships and project for a patch we'll import.
        t = Team.objects.create(name='foo')
        crowd = FakeCrowd()
        foo_group = [
            ('user@foo.com', 'user name'),
            ('user2@foo.com', 'user2 name'),
            ('zoltan.kiss@linaro.org', 'Zoltan Kiss'),
        ]
        crowd.add_group('foo', foo_group)
        sync_crowd(crowd, [t])

    # This can't be run against sqlite3, so it won't get hit by unit-test.sh.
    @skipUnlessDBFeature('supports_mixed_date_datetime_comparisons')
    def test_members_show(self):
        client = Client()
        resp = client.get('/team/foo/')
        self.assertEqual(resp.status_code, 200)
        self.assertIn('<h3>Current Members</h3>', resp.content)

    def test_user_show(self):
        client = Client()
        user = User.objects.get(email='zoltan.kiss@linaro.org')
        resp = client.get('/patches/%d/' % user.id)
        self.assertEqual(resp.status_code, 200)
