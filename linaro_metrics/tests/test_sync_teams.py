from django.contrib.auth.models import User
from django.test import TestCase
from patchwork.models import Person

from linaro_metrics.models import Team, TeamMembership
from linaro_metrics.sync_teams import get_or_create_person, sync_crowd


class FakeCrowd(object):
    def __init__(self):
        self.groups = {}

    def add_group(self, name, users):
        self.groups[name] = users

    def get_group(self, name):
        return [x[0] for x in self.groups[name]]

    def get_user_no_cache(self, user_email):
        for users in list(self.groups.values()):
            for email, display_name in users:
                if user_email == email:
                    return {'display-name': display_name}


class TestSyncTeams(TestCase):
    def test_user_created(self):
        t = Team.objects.create(name='foo')
        crowd = FakeCrowd()
        crowd.add_group(t.name, [('user@foo.com', 'user name')])
        sync_crowd(crowd, [t])

        p = Person.objects.get(email='user@foo.com')
        self.assertEqual('user name', p.name)
        self.assertEqual(p.email, p.user.email)
        tm = TeamMembership.objects.all()[0]
        self.assertEqual('foo', tm.team.name)
        self.assertEqual(p.user, tm.user)

    def test_memberships_change(self):
        crowd = FakeCrowd()
        crowd.add_group('foo', [('user@foo.com', 'user name')])
        crowd.add_group('bam', [('user@foo.com', 'user name')])
        u = get_or_create_person(crowd, 'user@foo.com').user

        foo = Team.objects.create(name='foo')
        bar = Team.objects.create(name='bar')
        bam = Team.objects.create(name='bam')

        # put user in foo and bar
        TeamMembership.objects.create(team=foo, user=u)
        TeamMembership.objects.create(team=bar, user=u)

        # sync - and we should only be in foo and bam
        sync_crowd(crowd, [foo, bam])
        teams = [x.team.name for x in TeamMembership.objects.all()]
        self.assertEqual(['foo', 'bam'], teams)

    def test_person_no_user(self):
        crowd = FakeCrowd()
        crowd.add_group('foo', [('user@foo.com', 'user name')])
        Person.objects.create(name='user name', email='user@foo.com')
        u = get_or_create_person(crowd, 'user@foo.com').user
        p = Person.objects.get(email='user@foo.com')
        self.assertEqual(u, p.user)

    def test_user_no_person(self):
        crowd = FakeCrowd()
        crowd.add_group('foo', [('user@foo.com', 'user name')])

        orig = User.objects.create_user(
            'user name', 'user@foo.com', password=None)
        self.assertEqual(
            orig, get_or_create_person(crowd, 'user@foo.com').user)
