import json
import os
import tempfile
import time
import unittest

from linaro_metrics.crowd import Crowd


class TestCrowdCache(unittest.TestCase):
    def setUp(self):
        _, self.tmpfile = tempfile.mkstemp(prefix='crowdtest')
        self.addCleanup(os.unlink, self.tmpfile)
        self.crowd = Crowd('user', 'pass', 'https://foo/bar/server')

        def fake_get(api_url):
            return '''{"email": "email"}'''
        self.crowd._get = fake_get

    def test_unicode(self):
        '''Ensure we can handle unicode characters in an email address'''
        # a real commit in linux.git where the author has a unicode character:
        #  https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/
        #   commit/?id=e82661e23c60fc41424ca138820d729d8e4a2226
        self.crowd.get_user_no_cache('samuel.pitoiset\u0153gmail.com')

    def test_corrupted_cache(self):
        with open(self.tmpfile, 'w') as f:
            f.write('invalid json')
        with self.crowd.cached(self.tmpfile):
            self.assertTrue(self.crowd.user_valid('foo@bar.com'))
        with open(self.tmpfile) as f:
            self.assertIn('foo@bar.com', json.load(f))

    def test_cache_hit(self):
        data = {
            'foo@bar.com': {
                'email': 'foo',
                'valid': False,
                'expires': time.time() + 100,
            }
        }
        with open(self.tmpfile, 'w') as f:
            json.dump(data, f)
        with self.crowd.cached(self.tmpfile):
            self.assertFalse(self.crowd.user_valid('foo@bar.com'))

    def test_cache_miss(self):
        data = {
            'foo@bar.com': {
                'email': 'foo',
                'expires': time.time() - 1,
            }
        }
        with open(self.tmpfile, 'w') as f:
            json.dump(data, f)
        with self.crowd.cached(self.tmpfile):
            self.assertTrue(self.crowd.user_valid('foo@bar.com'))

    def test_cache_clean(self):
        data = {
            'foo@bar.com': {
                'email': 'email',
                'expires': time.time() - 1,
            },
            'FOO@bar.com': {
                'email': 'email',
                'expires': time.time() + 100,
            }
        }
        with open(self.tmpfile, 'w') as f:
            json.dump(data, f)
        with self.crowd.cached(self.tmpfile):
            pass
        with open(self.tmpfile) as f:
            self.assertEqual(1, len(list(json.load(f).keys())))
