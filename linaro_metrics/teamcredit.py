#!/usr/bin/env python
import datetime
from linaro_metrics.models import Team, TeamMembership, TeamCredit
from patchwork.models import Patch
from bin import django_setup
import logging
import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
django_setup()  # must be called to get sys.path and django settings in place

dateb = datetime.datetime.now() - datetime.timedelta(days=180)

log = logging.getLogger('teamcredit')

"""
This is a thrown together script to fix the teamcredit statistics. It should
not often be needed, but at times a ldap team can change and the team lead
forgets to inform patches of this change. Hence the teamcredit will go out of
sync.

The script runs in a default of 180 days.

"""

team = 'team-ldcg'
t = Team.objects.get(name=team)
tm = TeamMembership.objects.filter(team=t)

for p in tm:
    logging.info("#### %s %s", p.user, p.id)
    # get all patches to that user
    patchu = Patch.objects.filter(submitter__user=p.user)
    for pa in patchu:
        if dateb < pa.date:
            tcs = TeamCredit.objects.filter(patch=pa)
            if not tcs:
                logging.info("No Patch: %s, %s", pa, tcs)
                new_values = {
                    'patch': pa, 'team': t, 'last_state_change': pa.date
                }
                tcs = TeamCredit(**new_values)
                tcs.save()
            else:
                tcsno = TeamCredit.objects.filter(patch=pa,
                                                  team__name='no-team'
                                                  )
                if tcsno:
                    logging.info("No Credit: %s, %s", tcsno, pa.date)
                    TeamCredit.objects.filter(patch=pa).update(team=t)
