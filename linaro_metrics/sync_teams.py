#!/usr/bin/env python
import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from bin import django_setup, add_logging_arguments
django_setup()  # must be called to get sys.path and django settings in place

import logging

from django.conf import settings
from django.contrib.auth.models import User
from patchwork.models import Person

from linaro_metrics.crowd import Crowd, CrowdNotFoundException
from linaro_metrics.crowdwrap import CrowdWrap
from linaro_metrics.models import Team, TeamMembership

log = logging.getLogger('sync_teams')

DRY_RUN = False


def get_or_create_person(crowd, email, save_person=True):
    name = None
    try:
        person = Person.objects.get(email__iexact=email)
    except Person.DoesNotExist:
        # use crowd to get the "display-name" for the user
        name = crowd.get_user_no_cache(email)['display-name']
        log.info('Creating person %s(%s)', name, email)
        person = Person(name=name, email=email)
        if save_person:
            if not DRY_RUN:
                person.save()
            else:
                log.info(
                    "DRY-RUN: would have saved Person object for %s %s"
                    % (name, email))

    if not person.user:
        users = User.objects.filter(person__email=email)
        # If they aren't in the db, then let's try crowd
        if users.count() == 0:
            if not name:
                name = crowd.get_user_no_cache(email)['display-name']
            users = User.objects.filter(username=name)

        # if there's still no user obj, create one
        if users.count() == 0:
            log.info('Creating user for %s', email)
            if not DRY_RUN:
                user = User.objects.create_user(name, email, password=None)
            else:
                user = None
        else:
            user = users[0]
        person.user = user

        if save_person:
            if not DRY_RUN:
                person.save()
            else:
                log.info(
                    "DRY-RUN: saving User(%s) to Person(%s)"
                    % (name, email))

    return person


def sync_team(crowd, team, emails, user_memberships):
    for email in emails:
        user = get_or_create_person(crowd, email).user
        user_memberships.setdefault(user, []).append(team)

        memberships = TeamMembership.objects.filter(team=team, user=user)

        # if no memberships were found, then we need to create it
        if memberships.count() < 1:
            if not DRY_RUN:
                membership = TeamMembership.objects.create(
                    team=team, user=user)
                if membership is not None:
                    log.info(
                        'New team membership on %s created for: %s' %
                        (team, email))
                else:
                    log.info(
                        'Failed to create team membership on %s for: %s' %
                        (team, email))
            else:
                log.info("DRY-RUN: would add user %s to team %s" %
                         (email, team))


def sync_crowd(crowd, teams):
    user_memberships = {}
    crowdwrap = CrowdWrap()

    for team in teams:
        try:
            emails = crowdwrap.get_group(crowd, team.name)
        except CrowdNotFoundException:
            log.error("Requested team '%s' not found" % team)
            continue

        log.info('syncing team: %s - (%s)', team, emails)
        sync_team(crowd, team, emails, user_memberships)
        if len(emails) == 0:
            log.warn('empty group definition in crowd for: %s', team)

    for user in User.objects.all():
        memberships = user_memberships.get(user, [])
        for tm in TeamMembership.objects.filter(user=user):
            if tm.team not in memberships:
                if not DRY_RUN:
                    log.warn('Deleting %s\'s membership in %s',
                             user.email, tm.team.name)
                    tm.delete()
                else:
                    log.info(
                        "DRY-RUN: would delete %s from %s" %
                        (user.email, tm.team.name))


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(
        description='Synchronize team memberships with info from crowd')
    add_logging_arguments(parser)
    parser.add_argument("--dry-run", "-n", action='store_true',
                        dest='DRY_RUN', default=False,
                        help="Run the script but do not execute any changes")
    args = parser.parse_args()

    DRY_RUN = args.DRY_RUN

    crowd = Crowd(settings.CROWD_USER, settings.CROWD_PASS, settings.CROWD_URL)
    sync_crowd(crowd, Team.objects.filter(active=True))
