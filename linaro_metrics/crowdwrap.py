#!/usr/bin/env python
import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
sys.path.append("/srv/linaro-git-tools")

from linaro_ldap import get_employees_by_team
from linaro_metrics.crowd import CrowdNotFoundException

PREFER_LDAP = ['LDCG']


class CrowdWrap(object):

    def get_group(self, crowd, grp):
        try:
            # in cases where there's actually a posix group
            # with the same name as the teamname, we need
            # to just skip crowd and go to LDAP.
            if grp not in PREFER_LDAP:
                return crowd.get_group(grp)
            else:
                raise CrowdNotFoundException
        except CrowdNotFoundException:
            # not in crowd, let's check LDAP
            ldap_teams = get_employees_by_team()
            for dept in ldap_teams:
                if (grp in ldap_teams[dept]):
                    # the function returns all LDAP data, so strip
                    # out just the 'mail' attribute for each entry
                    return [x["mail"] for x in ldap_teams[dept][grp]]

        # if we're still here, the group wasn't found
        raise CrowdNotFoundException('Group "%s" not found' % grp)
