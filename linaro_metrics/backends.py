from crowdrest.backend import CrowdRestBackend


class LowerCaseCrowdBackend(CrowdRestBackend):
    def create_or_update_user(self, user_id):
        return super(LowerCaseCrowdBackend, self).create_or_update_user(
            user_id.lower())

    def authenticate(self, username=None, password=None):
        return super(LowerCaseCrowdBackend, self).authenticate(
            username.lower(), password)
