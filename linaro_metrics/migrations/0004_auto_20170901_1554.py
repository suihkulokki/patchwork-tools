# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):
    atomic = False

    dependencies = [
        ('linaro_metrics', '0003_auto_20170525_1110'),
    ]

    operations = [
        migrations.AddField(
            model_name='committagcredit',
            name='date',
            field=models.DateTimeField(help_text=b'Used to help report by year/month', null=True),
        ),
        migrations.AddField(
            model_name='committagcredit',
            name='non_author',
            field=models.BooleanField(default=False, help_text=b'The tag was not from the author'),
        ),
    ]
