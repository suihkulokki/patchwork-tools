import datetime
import os

from django.db import migrations
from django.conf import settings

from dulwich.errors import NotGitRepository
from dulwich.repo import Repo


def insert_date(apps, schema_editor):
    if settings.DATABASES['default']['ENGINE'] == 'django.db.backends.sqlite3':
        print('skipping sqlite unit test')
        return

    ProjectTagCredit = apps.get_model('linaro_metrics', 'ProjectTagCredit')
    qs = ProjectTagCredit.objects.filter(
        commit_tag__date__isnull=True
    ).distinct(
        'commit_tag__commit', 'commit_tag__tag', 'commit_tag__person'
    )
    for c in qs:
        try:
            r = Repo(os.path.join(settings.REPO_DIR, c.project.linkname))
        except NotGitRepository:
            # probably came from github
            continue
        commit = r[c.commit_tag.commit.encode()]
        ts = datetime.datetime.fromtimestamp(
            commit.commit_time + commit.commit_timezone)
        c.commit_tag.date = ts
        # kind of a guess, but most efficient for a migration. Otherwise
        # you have to go through each linaro person attached to this
        # c.commit_tag.person.user and based on observation, emails vary
        # but core people keep their names the same.
        tagger = c.commit_tag.person.name
        if not tagger:
            print('No person name for', c.commit_tag.commit, c.commit_tag.email)
            continue
        author = commit.author.decode('utf-8', errors='replace')
        if not author:
            print('No author found for', c.commit_tag)
            continue
        c.commit_tag.non_author = tagger not in author
        c.commit_tag.save(update_fields=['non_author', 'date'])
        r.close()


def reverse_insert_date(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    atomic = False

    dependencies = [
        ('linaro_metrics', '0004_auto_20170901_1554'),
    ]

    operations = [
        migrations.RunPython(insert_date, reverse_code=reverse_insert_date),
    ]
