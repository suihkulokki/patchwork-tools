# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):
    atomic = False

    dependencies = [
        ('linaro_metrics', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='teamcredit',
            name='patch',
            field=models.ForeignKey(to='patchwork.Patch', on_delete=models.CASCADE),
        ),
    ]
