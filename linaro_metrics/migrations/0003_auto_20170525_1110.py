# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):
    atomic = False

    dependencies = [
        ('patchwork', '0015_add_series_models'),
        ('linaro_metrics', '0002_auto_20170412_1441'),
    ]

    operations = [
        migrations.CreateModel(
            name='CommitTagCredit',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tag', models.CharField(max_length=1024)),
                ('commit', models.CharField(max_length=1024)),
                ('person', models.ForeignKey(to='patchwork.Person', on_delete=models.CASCADE)),
            ],
        ),
        migrations.CreateModel(
            name='ProjectTagCredit',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('commit_tag', models.ForeignKey(to='linaro_metrics.CommitTagCredit', on_delete=models.CASCADE)),
                ('project', models.ForeignKey(to='patchwork.Project', on_delete=models.CASCADE)),
            ],
        ),
        migrations.CreateModel(
            name='TeamTagCredit',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('commit_tag', models.ForeignKey(to='linaro_metrics.CommitTagCredit', on_delete=models.CASCADE)),
                ('team', models.ForeignKey(to='linaro_metrics.Team', on_delete=models.CASCADE)),
            ],
        ),
        migrations.AlterField(
            model_name='teamcredit',
            name='patch',
            field=models.ForeignKey(to='patchwork.Patch', on_delete=models.CASCADE),
        ),
        migrations.AlterUniqueTogether(
            name='teamtagcredit',
            unique_together=set([('commit_tag', 'team')]),
        ),
        migrations.AlterUniqueTogether(
            name='projecttagcredit',
            unique_together=set([('commit_tag', 'project')]),
        ),
    ]
