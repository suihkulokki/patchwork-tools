# -*- coding: utf-8 -*-


from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):
    atomic = False

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('patchwork', '0003_add_check_model'),
    ]

    operations = [
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=1024)),
                ('display_name', models.CharField(max_length=1024)),
                ('active', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='TeamCredit',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('last_state_change', models.DateTimeField(help_text=b'Used to support time-to-acceptance metrics', null=True)),
                ('patch', models.ForeignKey(to='patchwork.Patch', on_delete=models.CASCADE)),
                ('team', models.ForeignKey(to='linaro_metrics.Team', on_delete=models.CASCADE)),
            ],
        ),
        migrations.CreateModel(
            name='TeamMembership',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('team', models.ForeignKey(to='linaro_metrics.Team', on_delete=models.CASCADE)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE)),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='teammembership',
            unique_together=set([('team', 'user')]),
        ),
        migrations.AlterUniqueTogether(
            name='teamcredit',
            unique_together=set([('patch', 'team')]),
        ),
    ]
