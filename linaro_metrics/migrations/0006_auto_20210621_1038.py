# Generated by Django 2.2.24 on 2021-06-21 10:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('linaro_metrics', '0005_populate_credit'),
    ]

    operations = [
        migrations.AlterField(
            model_name='committagcredit',
            name='date',
            field=models.DateTimeField(help_text='Used to help report by year/month', null=True),
        ),
        migrations.AlterField(
            model_name='committagcredit',
            name='non_author',
            field=models.BooleanField(default=False, help_text='The tag was not from the author'),
        ),
        migrations.AlterField(
            model_name='teamcredit',
            name='last_state_change',
            field=models.DateTimeField(help_text='Used to support time-to-acceptance metrics', null=True),
        ),
    ]
