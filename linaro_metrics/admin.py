from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html

from linaro_metrics.models import (
    Team,
    TeamMembership,
    TeamCredit,
    TeamTagCredit,
    CommitTagCredit,
)


class TeamAdmin(admin.ModelAdmin):
    list_display = ('name', 'display_name', 'active')
    readonly_fields = ('members',)
    ordering = ('name',)

    fieldsets = (
        (None, {
            'fields': ('name', 'display_name', 'active')
        }),
        ('Members', {
            'classes': ('collapse',),
            'fields': ('members',)
        }),
    )

    def members(self, team):
        qs = team.teammembership_set.get_queryset()
        output = ['<ul>']
        output += [
            format_html(
                '<li>{} &lt;{}&gt;</li>', x.person.name, x.person.email
            ) for x in qs
        ]
        output.append('</ul>')
        return '\n'.join(output)
    members.short_description = 'Members'


class TeamMembershipAdmin(admin.ModelAdmin):
    list_filter = ('team',)
    list_display = ('team', 'user')
    ordering = ('team', 'user')
    search_fields = ('team__name', 'user__username', 'user__email')


class TeamCreditAdmin(admin.ModelAdmin):
    list_display = ('team', 'patch')
    search_fields = ('team__name', 'patch__name')
    readonly_fields = ('hyper_patch', 'submitter')

    fields = ('team', 'hyper_patch', 'submitter')

    # A simple way to provide a hyperlink to the patch
    def hyper_patch(self, team_credit):
        patch = team_credit.patch
        meta = patch._meta
        link = reverse(
            'admin:%s_%s_change' % (meta.app_label, meta.module_name),
            args=(patch.id,))
        return format_html('<a href="{}">{}</a>', link, patch.name)
    hyper_patch.short_description = 'Patch'

    def submitter(self, team_credit):
        return team_credit.patch.submitter
    submitter.short_description = 'Submitter'


class TeamTagCreditAdmin(admin.ModelAdmin):
    ordering = ('team',)
    readonly_fields = ('commit_tag',)


class CommitTagCreditAdmin(admin.ModelAdmin):
    ordering = ('person',)


admin.site.register(Team, TeamAdmin)
admin.site.register(TeamMembership, TeamMembershipAdmin)
admin.site.register(TeamCredit, TeamCreditAdmin)
admin.site.register(TeamTagCredit, TeamTagCreditAdmin)
admin.site.register(CommitTagCredit, CommitTagCreditAdmin)
