from django.urls import re_path, include
from django.conf.urls import url
from django.contrib import admin

import patchwork.urls

from linaro_metrics.api import TeamList, TeamDetail, TeamPatchList, TeamTagView
from linaro_metrics import views as linaro_views

admin.autodiscover()

urlpatterns = [
    # Provide our override views of things in patchwork
    re_path(r'^$', linaro_views.index_view, name='index_view'),
    re_path(r'^project/(?P<project_id>[^/]+)/list/$',
            linaro_views.project_view,
            name='project_view'),
    url(r'^register/', linaro_views.user_register, name='user_register'),

    # Include all the standard patchwork urls.
    url(r'^', include(patchwork.urls)),

    # Now provide our own urls.
    re_path(r'^faq$', linaro_views.faq_view, name='faq'),
    re_path(r'^team/$', linaro_views.team_overview, name='team_overview'),
    re_path(r'^projects/$', linaro_views.project_overview,
            name='project_overview'),
    re_path(r'^team/Landing Team - HiSilicon.*',
            linaro_views.team_view,
            {'team': 'Landing Team - HiSilicon/Huawei'}),
    re_path(r'^team/(?P<team>[^/]+)/$', linaro_views.team_view,
            name='team_view'),
    re_path(r'^patches/(?P<user>[^/]+)/$', linaro_views.user_view,
            name='user_view'),
    re_path(r'^reports/project_activity$',
            linaro_views.report_project_activity,
            name='report_project_activity'),
    re_path(r'^reports/non-author-sign-offs$',
            linaro_views.report_signed_off_non_author,
            name='report_signed_off_non_author'),
    url(r'^api/1.0/teams/$', TeamList.as_view(), name='api-team-list'),
    url(r'^api/1.0/teams/(?P<pk>[^/]+)/$', TeamDetail.as_view(),
        name='api-team-detail'),
    url(r'^api/1.0/teams/(?P<team_id>[^/]+)/patches/$',
        TeamPatchList.as_view(), name='api-team-patch-list'),
    url(r'^api/1.0/teams/(?P<team_id>[^/]+)/commit-tags/$',
        TeamTagView.as_view(), name='api-team-credits'),

    # compatibility for old patches
    re_path(r'^(?P<patch>\d+)/$', linaro_views.old_patch_link,
            name='old_patch_link'),
]
