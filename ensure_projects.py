#!/usr/bin/python

from bin import django_setup
from django.conf import settings
from patchwork.models import Project

django_setup()  # must be called to get sys.path and django settings in place

for p in settings.DEFAULT_PROJECTS:
    try:
        Project.objects.get(linkname=p['linkname'])
    except Exception:
        print('Creating project: %s' % p['linkname'])
        Project.objects.create(**p)
